var searchData=
[
  ['speedofsound_0',['speedOfSound',['../d4/d01/class_train.html#ae1b4c67ea850bc3862d1db8471fd61b0',1,'Train']]],
  ['speedvariation_1',['speedVariation',['../d8/d2d/class_net_link.html#a8e37c379126a4dcc80a1cae80d914fd1',1,'NetLink']]],
  ['spinbox_5fplotevery_2',['spinBox_plotEvery',['../d5/dac/class_netrainsim__ui_1_1_ui___ne_train_sim.html#a150a5e7490528e3aef7fe24737818cb5',1,'Netrainsim_ui::Ui_NeTrainSim']]],
  ['startendpoints_3',['startEndPoints',['../d4/d01/class_train.html#a9abbfe248fd9472bb6ef2eb0a58af130',1,'Train']]],
  ['statusbar_4',['statusbar',['../d5/dac/class_netrainsim__ui_1_1_ui___ne_train_sim.html#a00805dcaab595c898110d55c9c7ea414',1,'Netrainsim_ui::Ui_NeTrainSim']]],
  ['stoppedstat_5',['stoppedStat',['../d4/d01/class_train.html#a0f511f02fba3d6803b43824443b11c65',1,'Train']]],
  ['stoptrainifnoenergy_6',['stopTrainIfNoEnergy',['../d4/d01/class_train.html#afa0e30ce6f070235a102207f5869e56f',1,'Train']]]
];
